module.exports = {
  title: 'Ifapme X44',
  description: 'Support de cours',
  markdown: {
    // options for markdown-it-anchor
    lineNumbers: false,
    /* anchor: { permalink: false },
    // options for markdown-it-toc
    toc: { includeLevel: [1, 2] },
    config: md => {
      // use more markdown-it plugins!
      md.use(require('markdown-it-xxx'))
    } */
  },
  themeConfig: {
    lastUpdated: 'Dernière mise à jour', // string | boolean
    displayAllHeaders: false, // Default: false
    // Assumes GitHub. Can also be a full GitLab url.
    repo: 'https://gitlab.com/g.guminski/ifapme-x44',
    // Customising the header label
    // Defaults to "GitHub"/"GitLab"/"Bitbucket" depending on `themeConfig.repo`
    repoLabel: 'Contribuer !',
    // defaults to false, set to true to enable
    editLinks: true,
    // custom text for edit link. Defaults to "Edit this page"
    editLinkText: 'Aidez-nous à améliorer cette page !',
    nav: [
      {text: 'Home', link: '/'},
      {text: 'Roadmap', link: '/roadmap/'},
      {
        text: 'Introduction',
        items: [
          { text: 'IDE & Visual Studio Code', link: '/introduction/01-visual-studio.html' },
          { text: 'Guide de création de Sites Web', link: '/introduction/02-sites-web.html' },
          { text: 'Git & Gitlab', link: '/introduction/03-git.html' }
        ]
      },
      {
        text: 'Frontend',
        items: [
          { text: 'Les Balises HTML5', link: '/frontend/01-balises-html5.html' },
          { text: 'ARIA "Accessible Rich Internet Applications"', link: '/frontend/02-aria.html' },
          { text: 'Les microdatas', link: '/frontend/03-microdata.html' },
          { text: 'SEO', link: '/frontend/04-seo.html' },
          { text: 'Positionnement CSS', link: '/frontend/05-positionnement.html' },
          { text: 'Responsive & Media Queries', link: '/frontend/06-responsive.html' },
          { text: 'Fonts', link: '/frontend/07-font.html' },
          { text: 'Fichiers SVG et gestion d\'images', link: '/frontend/08-svg-image.html' },
          { text: 'Animation CSS', link: '/frontend/09-animate.html' },
          { text: 'Javascript avec jQuery', link: '/frontend/10-jquery.html' },
          { text: 'Laravel Mix', link: '/frontend/11-laravel-mix.html' },
          { text: 'Webdesign', link: '/frontend/12-webdesign.html' }
        ]
      },
      {text: 'Backend',
      items: [
        { text: 'Préparation', link: '/backend/01-preparation.html' },
        { text: 'Installation', link: '/backend/02-installation.html' },
        { text: 'Premiers pas avec Composer', link: '/backend/03-composer.html' },
        { text: 'Architecture de programmation & Laravel', link: '/backend/04-architecture.html' },
        { text: 'PHP Artisan & make:auth', link: '/backend/05-artisan.html' },
        { text: 'Routes', link: '/backend/06-routes.html' },
        { text: 'Views & Blade', link: '/backend/07-views.html' },
        { text: 'Migrations', link: '/backend/08-migrations.html' },
        { text: 'Model & Eloquent', link: '/backend/09-model.html' },
        { text: 'Controllers', link: '/backend/10-controllers.html' },
        { text: 'Route Model Binding', link: '/backend/11-route-model-binding.html' },
        { text: 'Forms & Requests', link: '/backend/12-form-and-requests.html' },
        { text: 'Validation', link: '/backend/13-validation.html' },
        { text: 'Eloquent Relationships', link: '/backend/14-eloquent-relationships.html' },
      ]},
      {text: 'Tags', link: '/tags/'},
      {text: 'À propos', link: '/about/'}
    ],
    sidebar: {
      '/introduction/' : [
        '',
        '01-visual-studio',
        '02-sites-web',
        '03-git'
      ],
      '/frontend/' : [
        '',
        '01-balises-html5',
        '02-aria',
        '03-microdata',
        '04-seo',
        '05-positionnement',
        '06-responsive',
        '07-font',
        '08-svg-image',
        '09-animate',
        '10-jquery',
        '11-laravel-mix',
        '12-webdesign'
      ],
      '/backend/' : [
        '',
        '01-preparation',
        '02-installation',
        '03-composer',
        '04-architecture',
        '05-artisan',
        '06-routes',
        '07-views',
        '08-migrations',
        '09-model',
        '10-controllers',
        '11-route-model-binding',
        '12-form-and-requests',
        '13-validation',
        '14-eloquent-relationships',
      ],
    }
  }
}
