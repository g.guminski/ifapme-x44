---
sidebarDepth: 2
tags: ["Laravel", "Model", "Eloquent"]
---

# Eloquent / Model

![Models Folder >](/images/backend/09-model/model-folder.png)

Maintenant que nous avons définit une table, nous allons créer le modèle s'y rapportant.

Les modèles vont permettre de définir tous les aspects de ce que l'on va trouver comme donnée et de ce que l'on va pouvoir en faire.

On pourra donc déterminer les champs et leurs valeurs, de la même manière qu'avec une table de migration. On va aussi déterminer si certaines données doivent être transformées avant de sortir.

On y établira les relations entre les différentes tables.

Enfin, on y créera toutes les requêtes à la base de données avec le Query Builder.

## Création

On commence évidemment par une commande php artisan qui va créer celui-ci.

```sh
php artisan make:model Lesson
```

Pour le moment, notre Model est vide :

```php
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    //
}

```

On pourrait croire qu'il ne sert à rien pour le moment mais grâce à sa création et au fait qu'il étende (extend) Model nous avons
accès à tout une série de fonction qui vont nous faciliter la vie dès que l'on voudra réaliser des opérations simples.

## Travailler avec son Model

Voici quelques fonctions utiles que l'on va utiliser avec notre Model :

```php
Lesson::get(); // Retourne toutes les lessons
Lesson::where('user_id', 1)->get(); // Retourne toutes les lessons que l'utilisateur 1 à créé
Lesson::first(); // Retourne la première lesson
Lesson::pluck('title'); // Retourne tous les titres de lessons
```

Créer des scopes / raccourcis pour se simplifier la vie
Lorsqu'on travaille avec Laravel, le but est de se simplifier la vie. C'est donc ce qu'on va essayer de faire dans la continuité.
Laravel nous permet de créer des « Scopes » autrement dit, des raccourcis qui vont nous permettre d'exécuter des requêtes de
manière centralisée.
[https://laravel.com/docs/5.7/eloquent#global-scopes](https://laravel.com/docs/5.7/eloquent#global-scopes)

On va donc pouvoir créer un Scope dans son Model pour, par exemple, récupérer toutes les Lesson qui sont actives. On ajoutera
donc cette fonction dans ce cas :

```php
public function scopeActive($query)
{
    return $query->where('active', 1);
}
```

Le fait d'écrire cette function dans notre Model va nous permettre d'exécuter cette fonction de cette manière :

```php
$lessons = App\Lesson::active()->get();
```

Afin de récupérer toutes les lessons actives. On va donc essayer au maximum d'utiliser ces scopes dès que possible afin de
simplifier l'affichage des autres pages.

Note : On aurait pu ajouter la fonction ``->get()`` directement dans le scope.

## Ressources

- [https://laravel.com/docs/5.7/eloquent](https://laravel.com/docs/5.7/eloquent)
- [https://laracasts.com/series/laravel-from-scratch-2018/episodes/7](https://laracasts.com/series/laravel-from-scratch-2018/episodes/7)