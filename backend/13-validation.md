---
sidebarDepth: 2
tags: ["Laravel", "route", "model", "controller", "validation"]
---

# Validation

Pour le moment, nous enregistrons les données sans vérifier ce qui est envoyé.
Vu qu'il ne faut jamais faire confiance à l'utilisateur, nous allons donc voir comment définir les méthodes de validation.

Nous pouvons définir des règles avec HTML5 comme en spécifiant un type de champ text, date… Rajouter l'attribut required…

Toutes ces règles sont très utiles lorsque l'on est dans le navigateur mais peuvent être très facilement contournée. On va donc toujours répéter ces règles en PHP vu que c'est PHP / Laravel qui va encoder les informations dans la base de donnée.

## Création des validations

Voici un exemple de validation

```php
public function store()
{
    $this->validate( request(), [
        'title' => 'required|alpha',
        'published' => 'boolean',
        'content' => 'required',
    ])

    Lesson::create(request([
        'title',
        'published',
        'content',
    ]))

    return redirect('/');
}
```
 
On va donc valider les données avant de les envoyer.
On peut donc spécifier plusieurs règles par champs en les séparant avec un |.
Toutes les règles de validations sont disponibles à cette adresse :
[https://laravel.com/docs/master/validation#available-validation-rules](https://laravel.com/docs/master/validation#available-validation-rules)

## Affichage des erreurs

Pour afficher les erreurs si nous avons un retour de Laravel, nous allons ajouter ce code sur notre page Blade afin de les afficher.

```php
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
```

## Ressources

- [https://laravel.com/docs/master/validation](https://laravel.com/docs/master/validation)
- [https://laracasts.com/series/laravel-from-scratch-2018/episodes/12](https://laracasts.com/series/laravel-from-scratch-2018/episodes/12)
