---
sidebarDepth: 2
tags: ["Laravel", "PHP", "Git", "Composer", "Laragon"]
---

# Installation

## Programmes nécessaires

Nous allons avoir besoin de 2 programmes afin de mettre en place Laravel. On va donc installer [Git](https://git-scm.com/) et [Composer](https://getcomposer.org/).

### [Git](https://git-scm.com/)

Git pour Windows est disponible à cette adresse : [https://git-scm.com/download/win](https://git-scm.com/download/win)<br/>
Pour Mac : [https://git-scm.com/download/mac](https://git-scm.com/download/mac)

Git va nous permettre de gérer les Repositories (les dépôts) que l'on peut trouver sur [Github](https://github.com/) et de réaliser quelques tâches annexes. Il va remplacer l'outil « Command » de Windows en nous permettant d'utiliser certains outils par ligne de commande.

#### Commandes utiles pour Git

- `ls` : Liste le contenu du dossier
- `cd nom_du_dosssier` : rentre dans un dossier
- `cd ..` : remonte d'un dossier (attention à l'espace entre cd et ..)

### [Composer](https://getcomposer.org/)

[Composer](https://getcomposer.org/) est un « plugin » pour [PHP](https://php.net) et va nous permettre de gérer tous les plugins que l'on va installer avec [Laravel](https://laravel.com/). Il va gérer toutes les dépendances de ces plugins pour faire en sorte que la mise à jour et leur installation se réalise de manière simple.

#### Commandes utiles pour Composer

- `composer update` : met à jour les plugins de notre projet qui sont listés dans le fichier composer.json
- `composer self-update` : permet de mettre à jour composer.

## Installation de Laravel

On va commencer par se rendre dans notre dossier [Laragon](https://laragon.org/)

```bash
C:\laragon\www
```

Afin de pouvoir installer facilement Laravel on va d'abord exécuter cette commande avec Git Bash (clique droit `Git Bash Here`) :

```bash
composer global require "laravel/installer"
```

Toujours dans notre dossier, on va maintenant créer notre application avec la commande suivante

```bash
laravel new nom_du_projet
```

## Configuration de Laragon

Suite à a création de notre projet, au prochain lancement de Laragon celui-ci va mettre à jour [le fichier host de Windows](https://www.google.be/search?q=windows+host+file&oq=windows+host+file) afin de rajouter un nom de domaine qui sera accessible avec le nom de votre projet.

Par exemple, si vous avez créé un dossier `ifapme`, votre site sera disponible via `http://ifapme."l'extension activée sur Laragon"`

## Configuration de Laravel

A la racine de notre projet Laravel se trouve un fichier `.env`.<br/>
Il correspond à la configuration de Laravel par rapport à son environnement.

En l'ouvrant, on va pouvoir modifier l'adresse par laquelle sera disponible notre application

```env
APP_URL=http://localhost/ifapme/
```

Et surtout, configurer l'accès à Mysql

```env
DB_DATABASE=nom_de_la_base
DB_USERNAME=root
DB_PASSWORD=
```

 Si la base de donnée n'est pas créée, utiliser l'interclassement suivant `utf8mb4_unicode_ci` et créer la table via [phpmyadmin](http://localhost/phpmyadmin/)

Nous sommes maintenant prêts à réaliser nos premières expérimentations sur Laravel !

## Ressources

- [https://laravel.com/docs/5.7/installation](https://laravel.com/docs/5.7/installation)
- [https://laravel.com/docs/5.7/configuration](https://laravel.com/docs/5.7/configuration)
- [https://laracasts.com/series/laravel-from-scratch-2018/episodes/1](https://laracasts.com/series/laravel-from-scratch-2018/episodes/1)
