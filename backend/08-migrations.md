---
sidebarDepth: 2
tags: ["Laravel", "Migration", "Mysql", "BDD", "Base de donnée"]
---

# Migrations

![Migrations Folder >](/images/backend/08-migrations/migrations-folder.png)

Nous avons une idée plus claire de la manière dont nous affichons et créons des pages. Nous allons donc faire le lien avec la base de données en créant d'abord un fichier de migration.

Les fichiers php de migration vont définir les tables et les champs de la base de donnée que nous allons utiliser.

Ils se trouvent dans le dossier database / migrations.

Les migrations sont un moyen simple de permettre la création de table mais aussi la vue rapide de la structure de notre base de données ainsi que de tous les changements (historique) qui ont été effectué.


## Création de table

Pour créer un fichier de migration nous allons utiliser php artisan.

```sh
php artisan make:migration
```

On va donc exécuter notre commande en lui donnant un nom descriptif de ce que l'on va faire, cela correspondra au nom du fichier php créé.
Dans ce cas si, nous allons créer notre première table (lessons), nous allons donc appeler notre migration : ``create_lessons_table``.
Nous allons aussi ajouter l'argument –create afin de préciser que nous souhaitons créer une table et pas simplement faire une
modification de notre

```sh
php artisan make:migration create_lessons_table --create=lessons
```

Si tout s'est bien passé, nous avons maintenant créé notre fichier de migration que nous allons éditer.

## Structure du fichier

Vu que les fichiers de migration sont présent afin de pouvoir exécuter des opérations et revenir en arrière, nous pouvons
constater que nous avons deux fonctions dans notre objet.

``Up()`` et ``Down()``.

On a donc la fonction ``up()`` qui va nous permettre d'exécuter ce que l'on souhaite et ``down()`` qui va revenir en arrière.
Il ne tient qu'à nous de déclarer ce que l'on souhaite faire dans les deux cas.
Si le but d'une migration est de créer un champ, nous aurons dans ``down()`` le code permettant d'enlever ce champ.

## Définition des champs de la table

Pour déterminer les champs ainsi que leur type, nous allons parcourir les différentes options grâce à leur liste disponible à cette adresse :

[https://laravel.com/docs/5.7/migrations#columns](https://laravel.com/docs/5.7/migrations#columns)

On va ainsi retrouver les classiques :

```php
$table->boolean('confirmed');
$table->integer('votes');
$table->string('email');
$table->text('description');
```

Mais aussi d'autres types plus particuliers à Laravel :

```php
$table->increments('id'); // Primary key qui sera unsigned par défaut !
$table->softDeletes();
```

Nous allons aussi pouvoir leur donner des propriétés comme par exemple :

```php
$table->text('description')->nullable(); // Permet au chant d'être de type null
$table->integer('votes')->default(1); // Assigne une valeur par défaut
$table->integer('user_id')->unsigned(); // N'autorise qu'une valeur positive
```

Celles-ci peuvent se suivre :

```php
$table->integer('votes')->default(1)->unsigned();
```

# Ressources

[https://laravel.com/docs/5.7/migrations](https://laravel.com/docs/5.7/migrations)
[https://laracasts.com/series/laravel-from-scratch-2018/episodes/6](https://laracasts.com/series/laravel-from-scratch-2018/episodes/6)