---
sidebarDepth: 2
tags: ["Laravel", "Apache", "MySQL", "Laragon", "Homestead"]
---

# Préparation

<TagLinks />

## Phase de réflexion

Avant de commencer à coder, il faut d'abord déterminer de manière claire et précise ce que l'on souhaite mettre en place dans notre application.
On peut donc noter les différentes pages disponibles et déterminer les fonctionnalités qui seront présentes.

Cela permet :

- une réflexion globale et en amont sur le projet
- de visualiser les étapes les plus compliquées/les plus simples
- de déterminer ce dont nous aurons besoin afin que tout fonctionne.

Quand on parle de besoin, on parle de dépendance. Par exemple, si on commence par créer une interface admin / backend, il faut avoir une page de login disponible. On appliquera donc des restrictions sur certaines pages selon que l'utilisateur soit connecté, possède le niveau suffisant pour accéder à la page...

## Phase de recherche

Une fois la liste des fonctionnalités établies, on vérifie qu'elles sont toutes réalisables avec les outils disponibles.
Laravel va nous permettre de réaliser beaucoup de tâches en ce qui concerne le traitement de données, gestion des sessions… On verra par la suite et plus en détail ce que le Framework pourra nous apporter.

Tout ce qui va sortir de l'ordinaire (par exemple : générer une facture en fichier PDF) peut demander l'utilisation d'outils externes. On peut aussi penser à des choses plus simples comme la gestion de dates avancées, le traitement de fichiers d'image, etc
On va alors obtenir une liste de « plugins » que l'on trouvera sur internet ou  que l'on devra développer.

Le but ici est de choisir ce que l'on va développer ou implémenter. Voir s'il y aura des modifications à apporter ou si tout est utilisable directement.

En ce qui concerne les plugins PHP, on va pouvoir les chercher sur Packagist, à l'adresse suivante : [https://packagist.org](https://packagist.org)
Comme tous plugins, on va regarder leurs fonctionnalités, s'ils sont en cours de développement, mis à jour.

## Recherche sur l'environnement de développement

Dans la pratique, on va toujours essayer de développer dans un environnement qui sera le plus proche de la réalité. Par exemple, si le site est mis en ligne sur un serveur [Ubuntu](https://www.ubuntu.com/) avec [Apache](https://httpd.apache.org/) et [MySQL](https://www.mysql.com/fr/), on va essayer de mettre en place une machine virtuelle qui aura la même configuration technique mais aussi logicielle afin de se préparer aux erreurs éventuelles dues aux limitations systèmes.

Par exemple, si la configuration [PHP](http://php.net/) de l'environnement de production limite l'envoi de fichier à 5 Mo, il est important d'avoir la même limitation dans notre environnement de développement.

L'environnement de développement et celui de production devront donc être les plus similaires possible.
Sur Windows, Laravel a mis au point [Homestead]((https://laravel.com/docs/5.7/homestead)) qui permet de se passer de toute configuration du serveur de développement.

[https://laravel.com/docs/5.7/homestead](https://laravel.com/docs/5.7/homestead)
Pour notre projet, nous allons continuer à travailler sur [Laragon](https://laragon.org/) et y installer [Laravel](https://laravel.com/).
