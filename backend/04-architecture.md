---
sidebarDepth: 2
tags: ["Laravel", "Model", "Vue", "Controller"]
---

# Architecture de programmation & Laravel

## MVC

L'architecture MVC désigne une manière de travailler qui permet de simplifier et organiser votre code.
Laravel utilise ce modèle et incorpore aussi d'autres idées qui permettent de rester organisé.

On aura donc :

- **M** pour __Model__
- **V** pour __View__
- **C** pour __Controller__

### Model

Le Model correspond aux données qui seront utilisées et la manière dont elles seront traitées.
On aura donc par un Model Facture dans lequel on déterminera :

Le nom de la table mysql utilisée par exemple, le nom du champ de la clé primaire, comment certains champs doivent être
transformé (boolean, int, date...). Les données sont donc déterminées mais aussi la manière de les utiliser.

Dans ce fichier, on aura aussi les requêtes qui permettront de travailler avec ce Model.

Par exemple :

- Une requête qui récupère les factures. Une requête qui les supprimes...

### Controller

Une fois que les données sont récupérées et définies, elles sont passées au Controller. Celui-ci va servir de lien entre le Model et la Vue. Il va transformer ou tester les données si nécessaires.

Dans le cas de données qui viennent d'un formulaire, on va valider les données avant des les envoyer dans la base de donnée.
On peut donc définir comment valider les données dans le Controller.

### View

Comme son nom l'indique, la View / Vue va permettre d'afficher les données pour l'utilisateur. Cela comprend donc tout ce qui
est code HTML, Javascript, CSS.

## Les Objets PHP et Laravel

Nous allons constamment travailler avec des Objets. Mais qu'est-ce que ça signifie ?

Un Objet, en programmation, est un élément qui va avoir des variables et des fonctions.
Si on devait faire un lien avec ce que l'on connait, on peut dire que l'ordinateur est un objet.

Il peut s'allumer, s'éteindre, lancer des programmes, des jeux... Il connait aussi notre login et notre mot de passe...

Nous pouvons aussi être considérer comme un objet. Nous pouvons marcher, courir, sauter, manger, dormir, jouer... Nous avons
aussi des choses que l'on peut considérer comme des variables : l'état de notre faim, fatigue...

Grâce à cette manière de penser, on va pouvoir définir ces objets et les utiliser indépendamment grâce à des fonctions et des
variables qui sont communes à tous les objets de la même famille.

### Namespace

```php
<?php

namespace App\Http\Controllers;
```

Tous les objets vont utiliser un Namespace. C'est très important car cela va permettre de les ranger à un endroit et donc de
pouvoir créer des objets qui ont le même nom mais qui sont dans des Namespace différent.

### Utiliser un autre Objet dans notre objet

Lorsque l'on va vouloir utiliser un autre objet, il va donc falloir utiliser le Namespace pour que PHP trouve l'objet que l'on
souhaite utiliser. On aura donc :

```php
\App\Lesson::get();
```

Pour éviter de spécifier tout le temps le Namespace dans un fichier, on va pouvoir indiquer celui-ci une fois au début de notre
fichier comme suite :

```php
<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\Illuminate\Http\Request;

class LessonController extends Controllers
{
    // ...
}
```

Grâce à use, j'ai définis que j'allais utiliser Lesson qui se trouve dans App. Je n'ai donc plus à indiquer le namespace à chaque fois que j'utilise ce Model.

```php
Lesson::get();
```

### Définir un objet (pas créer !)

Afin de définir un objet, nous allons utiliser les class comme suit :

```php
class LessonController extends Controllers
{
    // Mon objet LessonController
}
```

On définit donc une classe LessonController qui extends / étend la classe Controller.
Il n'est pas nécessaire d'étendre à partir d'une autre classe mais cela permet d'obtenir toute une série de fonctions / variables
propre à la classe que l'on étend. Dans ce cas si, on sous-entend qu'une classe Controller existe et que nous allons créer un Controller à partir de celle-ci.

### Les variables

Une des premières choses que l'on peut déclarer sont les variables qui vont composer notre objet.
Si j'utilise mon objet Lesson, je peux créer la variable $cast qui va me permettre de définir le type de certaine variable, grâce à Laravel. Par exemple :

```php
protected $casts = [
    'active' => 'boolean',
    'referral_id' => 'integer',
    'width' => 'integer',
    'height' => 'integer',
]
```

Ici la variable est protégée, c'est-à-dire que seul l'objet peut accéder à ces variables. Celles-ci ne sont donc pas publiques.

### Les fonctions

Nous pouvons maintenant créer des méthodes / fonctions qui seront utiles à notre objet. Par exemple, on pourrait avoir ceci :

```php
function manger () 
{
    // Ma fonction
}
```

On va utiliser en général deux types de fonctions, les fonctions publiques et privées. Les fonctions publiques sont accessibles à
tous, tandis que les privées ne peuvent être appelée qu'au sein de ce même objet. On va donc essayer au maximum de définir la
visibilité des fonctions selon leur utilité.

On pourrait donc avoir :

```php
public function manger () 
{
    // Ma fonction
}
```

### Comment accéder à un objet en tant qu'instance

La première solution est de l'instancier. Pour se faire nous allons créer notre objet et le stocker dans une variable.

```php
$lesson = new Lesson;
```

Nous pouvons maintenant appeler les variables et les fonctions qui le composent de cette manière :

```php
$lesson->create();
echo $lesson->titre;
```

### Les fonctions statiques

Parfois, nous avons besoin de créer des fonctions sans pour autant avoir besoin de toutes les informations de l'objet. Dans ce
cas, on va pouvoir créer des fonctions statiques qui vont s'écrire comme suit :

```php
public static function create () 
{
    // Ma fonction
}

Lesson::create();
```

On remarque ici l'utilisation des `::` à la place de `->`
Avec Laravel, on utilisera souvent cette manière d'écrire avec les Model et le Query Builder :

```php
Lesson::where('active', 1)->get();
```

On a donc un mélange des deux méthodes. En effet, l'objet Lesson n'est pas dans une variable donc on appelle la fonction static
Where, vu qu'il y a un chaînage des méthodes, l'objet sera automatiquement instancié grâce à Laravel, nous utilisons donc la ->
pour exécuter la/les méthodes suivantes.

## Ressouces

- [https://openclassrooms.com/fr/courses/1665806-programmez-en-oriente-objet-en-php/1665911-introduction-a-la-poo](https://openclassrooms.com/fr/courses/1665806-programmez-en-oriente-objet-en-php/1665911-introduction-a-la-poo)