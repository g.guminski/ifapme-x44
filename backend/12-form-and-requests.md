---
sidebarDepth: 2
tags: ["Laravel", "route", "model", "controller", "CSRF"]
---

# Formulaire et Requêtes

Maintenant que tout est en place, nous allons pouvoir créer nos formulaires afin de rentrer des données, les éditer…
Il y a plusieurs choses à respecter pour tirer profit des facilités de Laravel.

## Création du formulaire

### CSRF Field

Pour tout formulaire / requête que l'on va créer,  Laravel s'attend à recevoir un « CSRF Field ».
Il s'agit d'un « jeton », une référence grâce à laquelle Laravel va vérifier que le formulaire qui envoie une requête a bien été demander par la personne qui affiche le formulaire. 

Laravel fait donc en sorte que l'on arrive sur cette page :

- http://localhost/news/156/delete

Laravel regarde si le jeton est identique à ce qu'il attend afin de pouvoir valider l'opération demandée.

Nous allons donc ajouter cette ligne dans notre fichier Blade :

```html
<form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
{{ csrf_field() }}
```

C'est donc bien la dernière ligne qui va générer le champ pour nous.

```php
{{ csrf_field() }}
```


## Method Field

Nous avons vu avec les déclarations de routes et le principe CRUD que nous n'utilisons pas que des méthodes ``GET`` et ``POST`` (voir 10-Controllers.pdf). Nous allons aussi utiliser ``PUT/PATCH`` ainsi que ``DELETE``. Ces méthodes n'étant pas reconnues directement, nous devons ajouter un champ, comme pour la protection ``CSRF``, afin que Laravel reconnaisse notre requête comme tel.

Nous ajouterons donc

```php
{{ method_field('PUT') }}
```

En spécifiant le type de méthode en commentaire.

## Poster un formulaire – Controlleur et définition du Model

Pour ajouter les données nous avons plusieurs solutions. 

### Méthode Create

La méthode qui donne le plus de sécurité est celle-ci :

```php
public function store()
{
    Lesson::create(request()->all());

    return redirect('/'); // Redirige vers la homepage
    // return redirect()->back(); // revient à la page précédente
}
```

En utilisant Eloquent avec la méthode create(), nous allons automatiquement créer une leçon.

La méthode ``request()->all()`` permet de récupérer toutes les données du formulaire.<br/>
Dans ce cas si, nous allons devoir spécifier dans dans la page du Model les champs que l'on autorise à enregistrer de la sorte.

On ajoutera donc une variable protégée fillable comme ceci :


```php
class Lesson extends Model
{
    protected $fillable = ['title', 'content'];
}
```

 
On pourra aussi ajouter la variable guarded pour faire l'inverse, spécifier les champs que l'on autorise pas à être modifier via une formulaire.

Il s'agit donc d'un tableau que l'on remplira avec tous les noms de champs qui se trouvent dans notre formulaire et que l'on permet de spécifier. Si aucun champ n'est défini alors l'enregistrement échouera.

### Méthode Create spécifique

Dans cette version, nous spécifions directement les champs que nous attendons. 


```php
public function store()
{
    Lesson::create([
        'title' => request('title'),
        'content' => request('content'),
    ]);

    return redirect('/');
}
```

Ou bien 

```php
public function store()
{
    Lesson::create(
        request(['title', 'content'])
    );

    return redirect('/');
}
```

### Méthode Save

Pour finir, voici la version la plus longue qui montre ce que Laravel réalise comme opération pour nous.
 
```php
public function store()
{
    $lesson = new Lesson;

    $lesson->title = request('title');
    $lesson->content = request('content');

    $lesson->save();

    return redirect('/');
}
```

## Ressources

- [https://laravel.com/docs/master/request](https://laravel.com/docs/master/request)
- [https://laracasts.com/series/laravel-from-scratch-2018/episodes/11](https://laracasts.com/series/laravel-from-scratch-2018/episodes/11)

