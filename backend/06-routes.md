---
sidebarDepth: 2
tags: ["Laravel", "Route"]
---

# Routes

C'est parti pour du Laravel !

La première étape est de définir les adresses web / chemins / routes que l'on va utiliser.

On va donc pouvoir créer des pages comme :
[http://ifapme.dev/laravel-est-genial](http://ifapme.dev/laravel-est-genial)
ou
[http://ifapme.dev/admin/post/create](http://ifapme.dev/admin/post/create)

Grâce à une simple ligne définie dans le [fichier .htaccess](https://fr.wikipedia.org/wiki/Discussion:.htaccess) :

```sh
RewriteRule ^(.*)/$ /$1 [L,R=301]
```

## Où définir les routes ?

![Routes Folder >](/images/backend/06-routes/routes-folder.png)

Lorsque l'on regarde notre projet Laravel, on se retrouve avec beaucoup de dossier que l'on
comprendra petit à petit.

Ici, on va travailler avec les routes et on a justement un dossier routes :
Dans le dossier routes on a de nouveau plusieurs fichiers

Ce qui va nous intéresser dans un premier est le fichier web.php
Il va en effet contenir toutes les routes utilisées pour accéder au site web.

## Types de requêtes RESTful

Le type de requête RESTful est une méthode moderne pour déterminer et différencier le type de requêtes. Cela permet aussi
d'harmoniser et de permettre un accès externe à votre API / système. Si un jour vous souhaiter qu'un serveur externe puisse
exécuter certaines actions, il utilisera les requêtes définies comme suit :

- `GET` : Méthode par laquelle on va accéder aux données.
- `PUT` : Méthode qui permet d'ajouter un élément dans la base de données
- `POST` : Méthode qui permet d'ajouter plusieurs éléments dans la bdd.
- `PATCH` : Lorsque l'on souhaite éditer un élément
- `DELETE` : Lorsque l'on souhaite supprimer un élément.

## Routes actuellement définies et utilisation d'objets

```php
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
```

La première chose que l'on remarque grâce aux couleurs c'est que l'on a 3 groupes définis.

Regardons de plus prêt la dernière route déclarée :

- `Route` est un objet. Il est écrit en jaune. Le nom est commencé par une majuscule.
- `GET` est une fonction statique de cet objet car on a les `::` qui lient le nom de l'objet et la méthode / fonction.
- `GET` correspond au type de requête avec laquelle on souhaite travailler (voir types de requêtes).
Une fonction statique d'un objet veut dire que l'on n'a pas besoin de créer l'objet pour utiliser cette fonction.
On peut donc déclarer autant de route que l'on souhaite de cette manière.
- `'/home'` correspond au chemin de l'url que l'on définit. Ce qui veut dire qu'on aura cette adresse créée :
[http://ifapme.dev/home](http://ifapme.dev/home)
- `'HomeController@index'` fait référence au Controller qui sera utilisé et index la méthode/fonction qui sera appelée
à l'intérieur de celui-ci.

Nous avons aussi 2 autres choses définies :

```php
Auth::routes();
```

Va créer toutes les routes qui seront utilisées pour se connecter, déconnecter, retrouver le mot de passe perdu...

```php
Route::get('/'...
```

Va renvoyer la View welcome. C'est donc cette ligne qui affiche la page que l'on voit actuellement lorsqu'on essaie
d'accéder au site.

## Un test simple

Pour tester la création d'une route simple, ajoutons une ligne comme ce qui suit :

```php
Route::get('/test', function () {
    return 'test';
});
```

Lorsque j'accède à ma page avec l'adresse : [http://ifapme.dev/test](http://ifapme.dev/test)

Je peux voir qu'il est écrit test.

On peut donc créer des pages très simplement grâce à ce type de code. Pas besoin de créer des fichiers PHP pour ça, tout est fait
automatiquement dans Laravel.

## Utilisation dans l'absolu

Dans l'absolu, on déclarera nos routes de la même manière que la ligne qui suit :

```php
Route::get('/test', 'HomeController@index');
```

Cela nous permet d'être organisé car on saura directement où aller chercher notre contenu.
Dans la méthode index qui se trouve dans le Controller HomeController.

## Routes de type REST

Le principe REST est commun aux [API](https://fr.wikipedia.org/wiki/Interface_de_programmation) et donc très souvent utilisé.
Cette convention permet de réaliser toutes les commandes qui nous permettent de travailler avec un modèle.

Verb      | URI                  | Action       | Route Name
----------|-----------------------|--------------|---------------------
GET       | `/users`              | index        | users.index
GET       | `/users/create`       | create       | users.create
POST      | `/users`              | store        | users.store
GET       | `/users/{user}`      | show         | users.show
GET       | `/users/{user}/edit` | edit         | users.edit
PUT/PATCH | `/users/{user}`      | update       | users.update
DELETE    | `/users/{user}`      | destroy      | users.destroy

On va pouvoir déclarer les routes de deux manières, soit une à une :

```php
/*
|--------------------------------------------------------------------------
| Users Routes
|--------------------------------------------------------------------------
*/
Route::get('users', 'UserController@index');
Route::put('users', 'UserController@store');
Route::get('/users/{user}/edit', 'UserController@edit');
Route::patch('/users/{user}/edit', 'UserController@update');
Route::delete('/users/{user}', 'UserController@destroy');
```

Soit grâce aux [routes resources](https://laravel.com/docs/5.7/controllers#resource-controllers) qui enregistrent automatiquement toutes les routes de type REST.

```php
/*
|--------------------------------------------------------------------------
| Users Routes
|--------------------------------------------------------------------------
*/
Route::resource('users', 'userController');
```


## Resources

- [https://laravel.com/docs/5.7/routing](https://laravel.com/docs/5.7/routing)
- [https://laracasts.com/series/laravel-from-scratch-2018/episodes/2](https://laracasts.com/series/laravel-from-scratch-2018/episodes/2)