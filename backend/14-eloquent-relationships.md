---
sidebarDepth: 2
tags: ["Laravel", "route", "model", "controller", "validation"]
---

# Eloquent Relationships

Dans le cadre de mon exercice personnel, un problème survient. Je gère des leçons qui appartiennent à des cours.
Nous allons voir comment nous allons pouvoir lier ceux-ci afin de le retrouver facilement avec Laravel.

J'ai donc :

- Un Model Lesson ainsi qu'un Model Subject.
- Un sujet peut avoir plusieurs leçons, une leçon appartient à un sujet.

## Migration

Afin de définir qu'une leçon peut appartenir à un cours, j'ai besoin d'un champ dans la base de données qui me servira de lien.

J'ajouterais donc cette ligne :

```php
$table->integer('subject_id')->index();
```
 
J'ai précisé que ce champ aura un index afin d'accélérer les recherches en le mettant en mémoire.

Pour aller plus loin, on peut aussi établir une référence pour que Mysql ou autre (le système de gestion de base de données) comprenne que deux tables sont liées par un champ.

On pourra donc ajouter aussi :

```php
$table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
```

Ici, j'ai ajouté l'option onDelete -> cascade pour définir que si un sujet est supprimé, tous les cours qui sont liés le seront aussi.
C'est bien le système de gestion de base de données (Mysql dans notre cas) qui effectuera automatiquement cette tâche.

## Mise à jour des Models et exécution

### HasMany

Afin de pouvoir utiliser ce lien dans nos Controlleur, nous allons définir celui-ci dans chaque Model.

```php
class Subject extends Model
{
    public function lessons()
    {
        return $this->hasMany(Lesson::class);
    }
}
```
 
Je définis ici le lien qui permet à Laravel de comprendre qu'un Sujet à plusieurs Leçons.
Lorsque j'aurais un Sujet, je pourrais ressortir toutes les Leçons en utilisant :

```php
$subject->lessons;
``` 
 
### belongsTo

On définit la relation inverse comme ceci

```php
class Lesson extends Model
{
    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }
}
```
 
De la même manière, on pourra afficher le Sujet d'une Leçon de cette manière :

```php
$lesson->subject;
```
 
### Many to Many -> relation de type Tags

Cette relation part du principe que les Tags peuvent être associés plusieurs fois à une Leçon et une Leçon peut avoir plusieurs tags. On a donc une multiplicité des relations possibles.

#### Migration

Créons d'abord notre table de Tags ainsi qu'une table Pivot qui va nous server à stocker tous les liens possible :

```php
class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table)) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->timestamps();
        }

        Schema::create('lesson_tag', function (Blueprint $table)) {
            $table->integer('lesson_id')->index();
            $table->integer('tag_id')->index();
            $table->primary(['lesson_id', 'tag_id']);
        }
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropifExists('tags');
        Schema::dropIfExists('lesson_tags');
    }
}
```
 
Nous avons ajouter une clé primaire qui comprend les deux champs de la table pivot afin que les liens restent uniques.
Nous n'avons pas ajouté de clé étrangère (foreign) vu que ce n'est pas nécessaire pour Laravel mais seulement pour Mysql (dans notre cas).<br/>
Attention que la manière de nommer ces éléments facilitera la compréhension automatique par Laravel.
 
## Model et execution

Vu que l'on est dans une relation Many to Many, nous utilisons la même fonction pour définir la relation qui lie chaque Model :

```php
class Lesson extends Model
{
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
```

```php
class Tag extends Model
{
    public function lessons()
    {
        return $this->belongsTo(Lesson::class);
    }
}
```
 
Pour récupérer simplement tous les éléments en une fois, c'est à dire prendre tous les leçons et récupérer tous les tags associés, on pourra travailler de cette manière :

```php
$lessons = Lesson::with('tags')->get();
```
 
En une ligne, Laravel récupère tous les éléments assemblés.

Pour créer et défaire les liens, on pourra utiliser ces commandes :

```php
$lesson->tags()->attach($tag);
$lesson->tags()->detach($tag);
$lesson->tags()->attach([1, 2, 3]);
```

$tag correspondant à un objet de type Tag.

## Ressources

- [https://laravel.com/docs/master/eloquent-relationships](https://laravel.com/docs/master/eloquent-relationships)
- [https://laracasts.com/series/laravel-from-scratch-2018/episodes/15](https://laracasts.com/series/laravel-from-scratch-2018/episodes/15)
