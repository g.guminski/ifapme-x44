---
sidebarDepth: 2
tags: ["Laravel", "route", "model", "controller", "SEO"]
---

# Route Model Binding

Il s'agit d'un principe mis au point par Laravel afin de simplifier l'écriture des Routes ainsi que des Controllers.

Imaginons une route simple pour afficher une news, on pourrait avoir :

- http://localhost/news/4541
- http://localhost/news/ma-news

Dans le premier cas, on utilise un ``id`` dans le second, un ``slug``.

Dans Laravel, l'option par défaut sera d'utiliser les ``id``. Dans notre cas, vu que nous sommes dans le backend et que l'optimisation ``SEO`` a donc peu d'importance, nous allons rester avec les ``id``.

Si on souhaite utiliser une autre colonne par défaut, on pourra suivre la documentation à cette adresse : [https://laravel.com/docs/master/routing#route-model-binding](https://laravel.com/docs/master/routing#route-model-binding)

Voir « Customizing The Key Name.

## Passer une variable

On peut commencer par déclarer cette route :

```php
Route::get('/news/{id}'), 'NewsController@show');
```
 
Ce qui veut dire que dans notre controller,  nous allons recevoir un variable avec le nom ID que l'on pourra utiliser.

On aura donc :

```php
public function show($id)
{
    $news = News::find($id);

    // mes fonctions
}
```

## Passe un Model

Pour simplifier notre code, pourquoi pas spécifier que l'on attend directement une news.
Laravel va alors s'occuper de retrouver la news pour nous et de créer l'objet à notre place.
Evidemment, un Model News doit exister pour que cela fonctionne.

Notre route correspondra donc à :
 
```php
Route::get('/news/{news}'), 'NewsController@show');
```

Alors que dans notre controller nous aurons :
 
```php
public function show(News $news)
{
    // mes fonctions
}
```

## Ressources

- [https://laravel.com/docs/master/routing#route-model-binding](https://laravel.com/docs/master/routing#route-model-binding)
- [https://laracasts.com/series/laravel-from-scratch-2018/episodes/9](https://laracasts.com/series/laravel-from-scratch-2018/episodes/9)
