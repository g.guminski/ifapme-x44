---
sidebarDepth: 2
tags: ["Laravel", "View", "Blade"]
---

# Views & Blade

![Views Folder >](/images/backend/07-views/home.blade.png)

Maintenant que l'on a vu comment créer des routes, regardons comment travailler avec les Views.

On va donc dans le dossier resources et views.

On y trouve des fichiers php avec une autre extension Blade !

Avant d'ouvrir votre fichier blade, vérifier bien que l'extension Laravel Blade est installée pour

votre éditeur. C'est toujours plus simple avec les couleurs.

## Ouverture du premier fichier Blade home.blade.php

>seulement présent avec make:auth

```php
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
```

Grâce aux couleurs on peut directement distinguer le code HTML du code Blade.

## Blade en général

- `@` : de manière générale c'est le signe distinctif que l'on travaille avec une fonction Blade.
- `@extends('layouts.app')` : Va exécuter le fichier app.blade.php qui se trouve dans le dossier layouts. Cela va nous permettre de gérer des templates pour ne pas réécrire le même code HTML tout le temps.
- `@section('content')
@endsection` : Délimitent une section qui est définie, dans ce cas si, dans le fichier app.blade.php par la commande
`@yield('content')`

## Afficher du PHP

Nous avons plusieurs possibilitées pour afficher du PHP.

```php
{{ $var }}
```

Cette première solution va permettre d'afficher une variable tout en étant protégé d'attaque XSS.
Les attaques XSS affichent du code HTML / Javascript non attendu.

```php
{!! $slot !!}
```

Ici, on va afficher le code sans protection pour les attaques XSS. Il faut donc être certains de ce que l'on souhaite afficher.

```php
@php
    // Code
@endphp
```

La dernière méthode affiche du code PHP normalement. On va aussi éviter ce genre de code et essayer de déplacer tout logique
PHP dans le Controlleur.

## Ressources

[https://laravel.com/docs/5.7/views](https://laravel.com/docs/5.7/views)
[https://laravel.com/docs/5.7/blade](https://laravel.com/docs/5.7/blade)
[https://laracasts.com/series/laravel-from-scratch-2018/episodes/2](https://laracasts.com/series/laravel-from-scratch-2018/episodes/2)
[https://laracasts.com/series/laravel-from-scratch-2018/episodes/10](https://laracasts.com/series/laravel-from-scratch-2018/episodes/10)