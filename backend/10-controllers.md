---
sidebarDepth: 2
tags: ["Laravel", "Controller", "Crud", "Route", "Resource"]
---

# Controllers

![Controllers Folder >](/images/backend/10-controllers/controllers-folder.png)

Tout est maintenant en place pour pouvoir utiliser les Controllers et lier, de manière organisée,  les requêtes de notre Model avec les Routes que nous avons créé.

## Création du Controller

On commence évidemment par une commande php artisan qui va créer celui-ci.

```sh
php artisan make:controller LessonController
```
 
Si on regarde notre Controller, celui-ci est vide.

## CRUD / REST dans le Controller

CRUD est un principe qui définit la manière dont nous allons interagir avec notre Model.<br/>
Cela induit une nomenclature particulière pour réaliser des opérations de bases.<br/>
Create, Read, Update, Delete.

En voici un tableau récapitulatif :

Verb      | URI                  | Action       | Route Name
----------|-----------------------|--------------|---------------------
GET       | `/photos`              | index        | photos.index
GET       | `/photos/create`       | create       | photos.create
POST      | `/photos`              | store        | photos.store
GET       | `/photos/{photo}`      | show         | photos.show
GET       | `/photos/{photo}/edit` | edit         | photos.edit
PUT/PATCH | `/photos/{photo}`      | update       | photos.update
DELETE    | `/photos/{photo}`      | destroy      | photos.destroy

[https://laravel.com/docs/5.7/controllers#resource-controllers](https://laravel.com/docs/5.7/controllers#resource-controllers)

### Verb

Cette colonne correspond au type de requête qui va être utilisé. Nous avons déjà vu les requêtes de type ``GET`` (qui récupèrent des données, mais aussi ``POST`` afin d'envoyer les données d'un formulaire.

! Attention ! En HTML, on ne pourra pas spécifier directement l'utilisation des méthodes ``PUT``, ``PATCH`` et ``DELETE``. Laravel permet d'utiliser ces méthodes avec un formulaire grâce à cette ligne :

```php
{{ method_field('PUT') }}
```
### URI

C'est la partie de l'url qui va nous dire à quel moment l'action (fonction) sera exécutée.<br/>
Lorsque j'irai sur ``https://mondomaine.com/photos/create`` j'exécuterais la fonction ``create`` de mon controller

### Action

Celle-ci correspond aux noms des fonctions qui apparaitront dans notre Controller.

### Route Name

Correspond au nom de la route qui sera accessible partout dans mon application.<br/>
On pourra donc utiliser ``route('photos.create')`` afin de récupérer l'adresse ``https://mondomaine.com/photos/create``.

### Controller ressource

Et si on pouvait créer une Controller avec cette manière de travailler ? Simple !

```sh
php artisan make:controller LessonController --resource
```

Si nous retournons voir notre Controller, on peut constater que les fonctions créées sont celle de notre tableau. On peut maintenant travailler avec celles-ci en les modifiant pour vraiment donner vie à notre Controller.

## CRUD avec les Routes

Nous avons ici deux choix, soit on crée les routes manuellement, soit on les crée avec une ligne.
Si l'on souhaite activer toutes les routes ``CRUD``, on peut utiliser la ligne unique.

```php
Route::resource('photos', 'PhotoController');
```

Il faudra donc que notre Controller soit bien remplis afin de pouvoir fonctionner.
On peut aussi les créer partiellement en écartant certaines fonctions :

```php
Route::resource('photo', 'PhotoController', ['only' => [
    'index', 'show'
]]);

Route::resource('photo', 'PhotoController', ['except' => [
    'create', 'store', 'update', 'destroy'
]]);
```

Exemple de Route créée manuellement :

```php
Route::get('photos/{photo}/edit', 'PhotoController@edit');
```

On remarque deux choses ici :

- Le @ à coté du nom du Controller permet de spécifier le nom de la fonction qui sera appelée
- ``{photo}`` induit le fait que l'on s'attend à récupérer le Model Photo grâce à un ID ou un Slug selon la définition du Model.

## Utiliser notre Model avec le Controller, Route Model Binding

Laravel va nous simplifier la tâche encore une fois. En spécifiant directement le Model auquel on s'attend dans notre Controller,  Laravel va essayer de faire le lien de lui même. 

Regardons notre fonction ``show($id)``.

Si on inscrit à l'intérieur cette ligne :

```php
Return App\Lesson::find($id);
```

Vu que nous spécifions directement que nous attendons en retour un Model Lesson, Laravel va effectivement créer la recherche.
On peut simplifier ceci en modifiant l'entête de la fonction comme suit :

```php
Show(Lesson $lesson)
```

Laravel va alors s'occuper automatiquement de récupérer la Lesson qui nous intéresse.
On va donc pouvoir travailler avec la variable ``$lesson`` dans notre fonction pour la modifier ou en faire ce que l'on souhaite.
! La manière dont on nomme les variables est très importante et va permettre à Laravel de s'y retrouver. 

## Ressources

- [https://laravel.com/docs/5.7/controllers](https://laravel.com/docs/5.7/controllers)
- [https://laracasts.com/series/laravel-from-scratch-2018/episodes/8](https://laracasts.com/series/laravel-from-scratch-2018/episodes/8)
