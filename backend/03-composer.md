---
sidebarDepth: 2
tags: ["Laravel", "Composer", "Packagist"]
---

# Premiers pas avec Composer

## Mises à jours et entretien avec Composer, comprendre le fichier `composer.json`

Lorsque l'on regarde le contenu du fichier, on peut constater que plusieurs variables sont définies.

On a par exemple le `name` de notre application, avec une description…

On a aussi une variable require et require-dev qui détermine les pré requis nécessaire dont notre application a besoin pour fonctionner.

```php
"require": {
    "php": "^7.1.3",
    "fideloper/proxy": "^4.0",
    "laravel/framework": "5.7.*",
    "laravel/tinker": "^1.0"
}
```

On a donc besoin de php à la version minimale `7.1.3` et aussi `laravel/framework` avec `laravel/tinker`.<br/>
Grâce à la ligne `laravel/framework`, lorsque l'on exécutera la commande

```bash
composer update
```

Si aucune mise à jour n'est disponible voici le résultat que l'on devrait obtenir, quelque chose comme ce qui suit :

```bash
spyzo@SPYZOR-PC MINGW64 ~/Code/cyndash (master)
$ composer update
Loading composer repositories with package information
Updating dependencies (including require-dev)
Nothing to install or update
Generating optimized autoload files
> Illuminate\Foundation\ComposerScripts::postAutoloadDump
> @php artisan package:discover --ansi
Discovered Package: barryvdh/laravel-dompdf
Discovered Package: beyondcode/laravel-dump-server
Discovered Package: cviebrock/eloquent-sluggable
Discovered Package: cyndash/pdf-link
Discovered Package: cyndash/reference-gen
Discovered Package: davidpiesse/nova-toggle
Discovered Package: fideloper/proxy
Discovered Package: laravel/nexmo-notification-channel
Discovered Package: laravel/nova
Discovered Package: laravel/slack-notification-channel
Discovered Package: laravel/tinker
Discovered Package: martinlindhe/laravel-vue-i18n-generator
Discovered Package: mcamara/laravel-localization
Discovered Package: monarobase/country-list
Discovered Package: nesbot/carbon
Discovered Package: nunomaduro/collision
Discovered Package: spatie/laravel-tags
Discovered Package: spatie/laravel-translatable
Discovered Package: spatie/nova-tags-field
Discovered Package: tightenco/ziggy
Discovered Package: timothyasp/nova-badge-field
Package manifest generated successfully.
```

### Namespace & versioning

La première chose que l'on peut remarquer est l'utilisation du `/` dans les noms comme si on utilisait des dossiers.<br/>
Comme si `framework` se trouvait dans le dossier `laravel`.

```php
"laravel/framework": "5.7.*",
```

C'est exactement ça et ça s'appelle les [namespace](http://php.net/manual/fr/language.namespaces.php).<br/>
Ils permettent de spécifier un chemin pour accéder au code et donc d'organiser le code et ainsi éviter les ducplicatas. On pourra donc avoir deux « framework » qui seront chacun dans un namespace différent.

Dans le dossier racine de notre projet, composer vérifiera que la dernière version `5.7` est bien installée.

Le `5.7.*` détermine que Laravel se mettra à jour tant qu'il restera dans la version `5.7`. Le jour ou Laravel `5.8` arrivera, il faudra modifier notre fichier Composer.json pour changer les fichiers sources du Framework ( ! à bien regarder les indications sur la manière de mettre à jour le Framework !).

### Laravel Tinker & suite du versioning

```php
"laravel/tinker": "^1.0"
```

Tinker est un plugin qui va nous permettre d'exécuter du code PHP en ligne de commande. Très utile pour réaliser des tests rapides sur son application.
Ici, le symbole `~` permettra de mettre à jour le package tant qu'il reste en version 1.x. C'est un peu comme si on avait mis 1.*.

Pour déclarer les versions, on peut forcer l'utilisation d'une version en particulier en mettant directement « 1.0 » ou « master » par exemple. On n'aura donc plus droit à la « mise à jour automatique » mais ça permet d'éviter de rester avec un problème gênant dans une version trop récente du plugin.

On pourra trouver le listing des versions disponibles sur la page Github du package :

![Github Version](/images/backend/03-composer/github-version.png)

## Installer un plugin

![Packagist](/images/backend/03-composer/packagist.png)

Rendez-vous sur [Packagist](https://packagist.org/) et imaginons que l'on souhaite installer un package / plugin qui vient du site qui va nous permettre de générer un fichier PDF.

On va donc d'abord faire une recherche

![Packagist recherche PDF Laravel](/images/backend/03-composer/packagist-recherche-pdf-laravel.png)

En regardant les résultats, on va faire attention au nombre de téléchargements pour voir s'il est beaucoup utilisé.<br/>
On voit aussi rapidement si un package est réalisé pour Laravel 4 ou un autre Framework qui ne nous intéresse pas.

![Packagist page du package](/images/backend/03-composer/packagist-dompdf.png)

Sur la page du package on voit rapidement les statistiques générales du projet mais aussi les prérequis (php > 7...).

De plus, si je me rends sur [la page Github du projet](https://github.com/barryvdh/laravel-dompdf) :

![Packagist page du package](/images/backend/03-composer/github-dompdf.png)

On voit directement que :

- Le projet est toujours mis à jour.
- La dernière mise à jour date d'il y a 22 jours.
- Il y a 317 problèmes ouverts et la plupart d'entre eux ont des réponses.
- ...

On peut choisir de travailler avec des packages plus jeunes, donc avec beaucoup moins d'installations, car ils ont peut-être une nouvelle façon de répondre à nos besoins ou des fonctionnalités introuvables ailleurs.

Un package plus vieux, comme celui-ci, aura une plus grande communauté derrière. A nous de vérifier si elle est toujours active en cas de problème.

Maintenant que l'on a choisi notre package, on va pouvoir installer celui-ci de deux manières et les informations se trouvent en  descendant sur packagist.

On va exécuter cette commande renseignée dans le fichier `readme.md`

```sh
composer require barryvdh/laravel-dompdf
```

Notre premier package est donc maintenant installé et prêt à fonctionner.
On sait aussi comment mettre à jour les packages et sauter de version lorsque c'est nécessaire.

## Ressources

[https://laracasts.com/series/laravel-from-scratch-2018/episodes/1](https://laracasts.com/series/laravel-from-scratch-2018/episodes/1)
