---
sidebarDepth: 2
tags: ["Webdesign", "Web", "Charte graphique", "SVG", "Images", "Typo"]
---

# Webdesign

## Pourquoi

Il représente visuellement la marque/société/organisation, comme le fait un logo, en définissant l'identité visuelle et l'esthétique du produit. Il définit et présente les différentes fonctions d'ergonomies qui sont mises en place et permet aussi de comprendre celles-ci facilement en utilisant les standards du web.

Quelques mots qui définissent un webdesign :
Ergonomie, architecture, charte graphique, identité visuelle, marketing, interactivité...

<TagLinks />

## Les différentes étapes nécessaires à l'élaboration du design

### L'inspiration

Où en trouver ? - Les sites concurrents. - Les sites "bibliothèque de design" comme :

- [Google !!!](https://google.com/)
- [https://envato.com/](https://envato.com/)
- [http://www.thebestdesigns.com/](http://www.thebestdesigns.com/)
- [http://bestwebgallery.com](http://bestwebgallery.com)
- [http://www.csselite.com](http://www.csselite.com)
- [https://www.awwwards.com/](https://www.awwwards.com/)
- Etc...

#### Les éléments à prendre en compte

- Comment les menus et l'information sont mis en avant
- Qu'est-ce qui fait que le site paraît pro.
- Les idées d'interfaces qui peuvent être réutilisées.
- La disposition des éléments et la clarté du site.
- Ce qui ne fonctionne pas et pourquoi ?

### Agencement de la page / maquette

Créer un modèle de page en disposant les différents blocs / informations nécessaires.<br/>
Types d'information que l'on retrouve en général sur un site :

- Logo
- Menu
- Pied de page
- Fil d'Ariane
- Corps
- Publicité
- Bannière
- Intégration réseaux sociaux ...

#### Exemple maquette

![Maquette](/images/frontend/12-webdesign/maquette.png)

Grâce à un storyboard ou une maquette, on peut aussi définir les éléments qui seront animés (menu déroulant par ex., avec Javascript / Jquery) et la manière dont ils le seront.

### Charte graphique

La charte graphique est un document qui définit l'apparence, l'identité de la marque/société/organisation. Elle permet donc que chaque projet (site web, documents officiels, logo...) conserve la même identité et garde une certaine cohérence.

En général, elle va au moins définir les couleurs principales, la ou les typos utilisées ainsi que des pictos / formes qui vont faire partie de l’image de la société ou du projet.

#### Exemple charte graphique

![Charte Graphique](/images/frontend/12-webdesign/charte-graphique.png)

### Choix des couleurs

Bien que le choix des couleurs doit être définis par la charte graphique, il faut veiller à garder une certaine homogénéité et donc faire attention aux différentes associations que l'on peut faire.

En règle générale, on utilisera 2 à 3 teintes de couleurs différentes pour définir l'aspect du site. Il y aura toujours une teinte prédominante qui se trouve généralement dans le logo de la marque/société/organisation.

Définissez à l'avance une palette avec la / les couleurs principales et différentes intensités.
Cette palette sera utilisée pour faciliter les choix lors de la création du design.

#### Sites références de palette couleurs

- [http://www.colourlovers.com/](http://www.colourlovers.com/)
- [https://kuler.adobe.com/](https://kuler.adobe.com/)
- [http://www.colorschemedesigner.com/](http://www.colorschemedesigner.com/)
- [http://www.code-couleur.com/](http://www.code-couleur.com/)

#### Tableau récapitulatif de la signification associée aux couleurs

|Couleur | Symbolique méliorative |Symbolique péjorative |Domaine|
|---|---|---|---|
| Bleu | Calme, confiance, autorisation, apaisement, sérénité, protection, sérieux, mystique, bonté, eau, espace, paix | Froid, sommeil | Voile, nouvelles technologies, informatique, médecine|
| Violet | Délicatesse, passion, discrétion, modestie, religion. | Mélancolie, tristesse, deuil, insatisfaction | Culture, politique|
| Rose | Charme, intimité, femme, beauté | Naïveté | Journal personnel, femmes |
| Rouge | Chaleur, force, courage, dynamisme, triomphe, amour, enthousiasme. | Violence, colère, danger, urgence, interdit, sang, enfer. | Luxe, mode, sport, marketing, médias|
| Orange | Tiédeur, confort, gloire, bonheur, richesse, honneur, plaisir, fruit, odeur, tonus, vitalité. | Feu, alerte | Divertissement, sport, voyage|
| Jaune | Lumière, gaieté, soleil, vie, pouvoir, dignité, or, richesse, immortalité. | Tromperie, égoïsme, jalousie, orgueil, alerte |Tourisme|
| Vert | Nature, vie végétale, secours, équilibre, foi, apaisement, repos, confiance, tolérance, espoir, orgueil, jeunesse, charité. | | Découverte, nature, voyage, éducation |
| Brun | Calme, philosophie, terroir. | Saleté | Environnement |
| Blanc | Pureté, innocence, neige, propreté, fraîcheur, richesse. | | Mode, actualités |
| Gris | Neutralité, respect. | | Design, associations, organisations à but non lucratif |
| Noir | Sobriété, luxe, nuit. | Mort, obscurité, tristesse, monotonie. | Cinéma, art, photographie et interdit |

### Choix des images

Les sites d'aujourd'hui comportent de plus en plus d'images et c'est normal.

Elles donnent le ton du site et remplacent souvent un long texte d'introduction et viennent en complément d'un titre. Il peut être intéressant de les utiliser dans des bannières ou des carrousels. Il faut donc faire attention à la taille de celle-ci ainsi qu'à leur présentation afin de garder un style professionnel.

Souvenez-vous que :
>Une image vaut mieux qu'un long discours.

### Typographie

De manière générale, il est préférable d'utiliser une à deux polices différentes au maximum afin de garder une cohérence.

Les polices du web peuvent être utilisées mais ne sont pas toujours complètes. Elles donnent un poids supplémentaire non négligeable au chargement du site. On va donc préférer l'utilisation de police classique comme Verdana, Helvetica.

### Pictos / SVG

Toujours dans l'optique "Une image vaut mieux qu'un long discours." il est fort agréable d'utiliser des icônes / pictogrammes afin de faciliter et d' accélérer la lecture du site

On utilisera par exemple une maison à coté du bouton accueil, une enveloppe pour le bouton newsletter...

### Designer élément par élément

Ne démarrez pas en voulant designer le site entier directement.

Séparez chaque élément pour en faire une recherche particulière.
Appliquez ensuite les codes choisis (bords arrondis, ombres portées, couleurs...) qui définissent l'identité de votre design.

## Ressources

- [http://ergonomie-web.studiovitamine.com](http://ergonomie-web.studiovitamine.com)
- [http://le-webmaster.e-monsite.com/pages/content/cours-et-tutoriels/cours-gratuit-sur-le-design-web.html](http://le-webmaster.e-monsite.com/pages/content/cours-et-tutoriels/cours-gratuit-sur-le-design-web.html)
- [http://thenextweb.com/dd/2015/01/02/10-web-design-trends-can-expect-see-2015/](http://thenextweb.com/dd/2015/01/02/10-web-design-trends-can-expect-see-2015/)
